package com.example.sharifahmed.jobapplymemorandum.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.example.sharifahmed.jobapplymemorandum.Activity.JobApplyListsDetailsActivity;
import com.example.sharifahmed.jobapplymemorandum.Model.CandidateApplyInfo;
import com.example.sharifahmed.jobapplymemorandum.R;


import java.util.ArrayList;

/**
 * Created by SHARIF AHMED on 4/28/2017.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {

    ArrayList<CandidateApplyInfo> candidateApplyInfoList;
    Context context;

    public RecyclerAdapter(ArrayList<CandidateApplyInfo> candidateApplyInfoList,Context context) {

        this.candidateApplyInfoList=candidateApplyInfoList;
        this.context = context;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_row_layout,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view,context,candidateApplyInfoList);


        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {

        holder.jobPostTV.setText(candidateApplyInfoList.get(position).getJobPost());
        holder.companyNameTVt.setText(candidateApplyInfoList.get(position).getCompanyName());
        holder.jobLocationTV.setText(candidateApplyInfoList.get(position).getJobLocation());


    }

    @Override
    public int getItemCount() {
        return candidateApplyInfoList.size();
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {

        TextView jobPostTV;
        TextView companyNameTVt;
        TextView jobLocationTV;

        LinearLayout clickForDetails;
        ArrayList<CandidateApplyInfo> candidateApplyInfoList = new ArrayList<CandidateApplyInfo>();
        Context context;

        public RecyclerViewHolder(View itemView,Context context,ArrayList<CandidateApplyInfo>candidateApplyInfoList) {

            super(itemView);
            this.candidateApplyInfoList = candidateApplyInfoList;
            this.context = context;

            itemView.setOnClickListener(this);//Register The OnClickListener Interface

            jobPostTV = (TextView) itemView.findViewById(R.id.jobPostTextViewId);
            companyNameTVt = (TextView) itemView.findViewById(R.id.companyNameTextViewId);
            jobLocationTV = (TextView) itemView.findViewById(R.id.jobLocationtTextViewId);

            clickForDetails = (LinearLayout) itemView.findViewById(R.id.clickForDetailsId);
        }

        @Override
        public void onClick(View view) {

            int position = getAdapterPosition();

            CandidateApplyInfo candidateApplyInfo = this.candidateApplyInfoList.get(position);

            Intent goJobApplyListDetails = new Intent(this.context, JobApplyListsDetailsActivity.class);

            goJobApplyListDetails.putExtra ("Id",candidateApplyInfo.getCandidateId());
            goJobApplyListDetails.putExtra("CandidateName",candidateApplyInfo.getCandidateName());
            goJobApplyListDetails.putExtra("CompanyName",candidateApplyInfo.getCompanyName());
            goJobApplyListDetails.putExtra("JobPost",candidateApplyInfo.getJobPost());
            goJobApplyListDetails.putExtra("JobNature",candidateApplyInfo.getJobNature());
            goJobApplyListDetails.putExtra("JobLocation",candidateApplyInfo.getJobLocation());
            goJobApplyListDetails.putExtra("JobSalary",candidateApplyInfo.getJobSalary());
            goJobApplyListDetails.putExtra("JobDeadlineDate",candidateApplyInfo.getJobDeadlineDate());

            this.context.startActivity(goJobApplyListDetails);

        }
    }
}
