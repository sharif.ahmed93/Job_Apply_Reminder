package com.example.sharifahmed.jobapplymemorandum.Model;

/**
 * Created by SHARIF AHMED on 4/27/2017.
 */
public class CandidateApplyInfo {

    private int candidateId;
    private String candidateName;
    private String companyName;
    private String jobPost;
    private String jobNature;
    private String jobLocation;
    private String jobSalary;
    private String jobDeadlineDate;

    public CandidateApplyInfo() {

    }



    public CandidateApplyInfo(int candidateId, String candidateName, String companyName, String jobPost, String jobNature, String jobLocation, String jobSalary, String jobDeadlineDate) {
        this.candidateId = candidateId;
        this.candidateName = candidateName;
        this.companyName = companyName;
        this.jobPost = jobPost;
        this.jobNature = jobNature;
        this.jobLocation = jobLocation;
        this.jobSalary = jobSalary;

        this.jobDeadlineDate = jobDeadlineDate;
    }


    public CandidateApplyInfo(String candidateName, String companyName, String jobPost, String jobNature, String jobLocation, String jobSalary, String jobDeadlineDate) {
        this.candidateName = candidateName;
        this.companyName = companyName;
        this.jobPost = jobPost;
        this.jobNature = jobNature;
        this.jobLocation = jobLocation;
        this.jobSalary = jobSalary;
        this.jobDeadlineDate = jobDeadlineDate;
    }


    public int getCandidateId() {
        return candidateId;
    }

    public String getCandidateName() {
        return candidateName;
    }

    public void setCandidateName(String candidateName) {
        this.candidateName = candidateName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getJobPost() {
        return jobPost;
    }

    public void setJobPost(String jobPost) {
        this.jobPost = jobPost;
    }

    public String getJobNature() {
        return jobNature;
    }

    public void setJobNature(String jobNature) {
        this.jobNature = jobNature;
    }

    public String getJobLocation() {
        return jobLocation;
    }

    public void setJobLocation(String jobLocation) {
        this.jobLocation = jobLocation;
    }

    public String getJobSalary() {

        return jobSalary;
    }

    public void setJobSalary(String jobSalary) {
        this.jobSalary = jobSalary;
    }

    public String getJobDeadlineDate() {
        return jobDeadlineDate;
    }

    public void setJobDeadlineDate(String jobDeadlineDate) {
        this.jobDeadlineDate = jobDeadlineDate;
    }
}
