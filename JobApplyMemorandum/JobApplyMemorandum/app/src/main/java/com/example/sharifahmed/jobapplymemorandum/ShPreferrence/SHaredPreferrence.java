package com.example.sharifahmed.jobapplymemorandum.ShPreferrence;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by SHARIF AHMED on 4/29/2017.
 */
public class SHaredPreferrence {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Context context;

    private static final String PREF_NAME = "userData";
    private static final String CANDIDATE_ID = "candidate_id";

    public SHaredPreferrence(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }
    public void saveUserData(int candidateId) {

        editor.putString(CANDIDATE_ID, String.valueOf(candidateId));
        editor.commit();
    }

    public int retriveMethod(){
        String DataToBeShow = sharedPreferences.getString(CANDIDATE_ID,"Data Not Found");
        int data = Integer.parseInt(DataToBeShow);
        return data;
    }

    public void deleteSharedData(){
        editor.clear();
        editor.commit();
    }


}
