package com.example.sharifahmed.jobapplymemorandum.DatabaseOperation;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.sharifahmed.jobapplymemorandum.DatabaseHelper.CandidateDatabaseHelper;
import com.example.sharifahmed.jobapplymemorandum.Model.CandidateApplyInfo;


import java.util.ArrayList;

public class CandidateDatabaseOperation {

    private CandidateDatabaseHelper databaseHelper;
    private SQLiteDatabase database;
    private CandidateApplyInfo candidateApplyInfo;


    public CandidateDatabaseOperation(Context context) {
        databaseHelper=new CandidateDatabaseHelper(context);
    }


    public void open(){

        database=databaseHelper.getWritableDatabase();

    }

    public void close(){

        databaseHelper.close();

    }

    public boolean addCandidate(CandidateApplyInfo candidateApplyInfo){

        this.open();

        ContentValues contentValues=new ContentValues();

        contentValues.put(CandidateDatabaseHelper.COL_CANDIDATE_NAME,candidateApplyInfo.getCandidateName());
        contentValues.put(CandidateDatabaseHelper.COL_COMPANY_NAME,candidateApplyInfo.getCompanyName());
        contentValues.put(CandidateDatabaseHelper.COL_JOB_POST,candidateApplyInfo.getJobPost());
        contentValues.put(CandidateDatabaseHelper.COL_JOB_NATURE,candidateApplyInfo.getJobNature());
        contentValues.put(CandidateDatabaseHelper.COL_JOB_LOCATION,candidateApplyInfo.getJobLocation());
        contentValues.put(CandidateDatabaseHelper.COL_JOB_SALARY,candidateApplyInfo.getJobSalary());
        contentValues.put(CandidateDatabaseHelper.COL_JOB_DEADLINE_DATE,candidateApplyInfo.getJobDeadlineDate());


        long inserted=database.insert(CandidateDatabaseHelper.TABLE_CANDIDATE_INFO,null,contentValues);

        this.close();

        if(inserted>0){
            return true;
        }else {
            return false;
        }

    }


    public CandidateApplyInfo getCandidateInfoById(int candidateId){

        this.open();

        Cursor cursor = database.query
                        (
                                CandidateDatabaseHelper.TABLE_CANDIDATE_INFO,
                                new String[]{CandidateDatabaseHelper.COL_CANDIDATE_NAME,
                                             CandidateDatabaseHelper.COL_COMPANY_NAME,
                                             CandidateDatabaseHelper.COL_JOB_POST,
                                             CandidateDatabaseHelper.COL_JOB_NATURE,
                                             CandidateDatabaseHelper.COL_JOB_LOCATION,
                                             CandidateDatabaseHelper.COL_JOB_SALARY,
                                             CandidateDatabaseHelper.COL_JOB_DEADLINE_DATE},
                                CandidateDatabaseHelper.COL_ID + " = " +candidateId,//Where Condition
                                null,
                                null,
                                null,
                                null
                        );
        cursor.moveToFirst();


        String candidateName = cursor.getString(cursor.getColumnIndex(CandidateDatabaseHelper.COL_CANDIDATE_NAME));
        String companyName = cursor.getString(cursor.getColumnIndex(CandidateDatabaseHelper.COL_COMPANY_NAME));
        String jobPost = cursor.getString(cursor.getColumnIndex(CandidateDatabaseHelper.COL_JOB_POST));
        String jobNature = cursor.getString(cursor.getColumnIndex(CandidateDatabaseHelper.COL_JOB_NATURE));
        String jobLocation = cursor.getString(cursor.getColumnIndex(CandidateDatabaseHelper.COL_JOB_LOCATION));
        String jobSalary = cursor.getString(cursor.getColumnIndex(CandidateDatabaseHelper.COL_JOB_SALARY));
        String jobDeadlineDate = cursor.getString(cursor.getColumnIndex(CandidateDatabaseHelper.COL_JOB_DEADLINE_DATE));

        candidateApplyInfo = new CandidateApplyInfo(candidateName,companyName,jobPost,jobNature,jobLocation,jobSalary,jobDeadlineDate);

        this.close();

        return candidateApplyInfo;
    }




    public ArrayList<CandidateApplyInfo> getAllCandidateInfo(){

        this.open();
        ArrayList<CandidateApplyInfo> candidateList = new ArrayList<>();

        //Cursor cursor = database.rawQuery("SELECT * FROM "+ CandidateDatabaseHelper.TABLE_CANDIDATE_INFO,null);

        Cursor cursor = database.query
                (
                        CandidateDatabaseHelper.TABLE_CANDIDATE_INFO,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null
                    );

        if(cursor!=null && cursor.getCount()>0){

            cursor.moveToFirst();

            for(int i=0;i<cursor.getCount();i++){

                int mId = cursor.getInt(cursor.getColumnIndex(CandidateDatabaseHelper.COL_ID));
                String candidateName = cursor.getString(cursor.getColumnIndex(CandidateDatabaseHelper.COL_CANDIDATE_NAME));
                String companyName = cursor.getString(cursor.getColumnIndex(CandidateDatabaseHelper.COL_COMPANY_NAME));
                String jobPost = cursor.getString(cursor.getColumnIndex(CandidateDatabaseHelper.COL_JOB_POST));
                String jobNature = cursor.getString(cursor.getColumnIndex(CandidateDatabaseHelper.COL_JOB_NATURE));
                String jobLocation = cursor.getString(cursor.getColumnIndex(CandidateDatabaseHelper.COL_JOB_LOCATION));
                String jobSalary = cursor.getString(cursor.getColumnIndex(CandidateDatabaseHelper.COL_JOB_SALARY));
                String jobDeadlineDate = cursor.getString(cursor.getColumnIndex(CandidateDatabaseHelper.COL_JOB_DEADLINE_DATE));

                candidateApplyInfo = new CandidateApplyInfo(mId,candidateName,companyName,jobPost,jobNature,jobLocation,jobSalary,jobDeadlineDate);
                candidateList.add(candidateApplyInfo);

                cursor.moveToNext();

            }
        }

        cursor.close();
        this.close();
        return candidateList;
    }

    public boolean deleteCandidate(int candidateId){

        this.open();

        int deleted = database.delete
                (
                        CandidateDatabaseHelper.TABLE_CANDIDATE_INFO,
                        CandidateDatabaseHelper.COL_ID + " = " +candidateId,
                        null
                );

        this.close();

        if (deleted>0){
            return true;
        }
        else {
            return false;
        }


    }

    public boolean updateCandidateInfoById(int candidateId,CandidateApplyInfo candidateApplyInfo){

        this.open();

        ContentValues contentValues=new ContentValues();

        contentValues.put(CandidateDatabaseHelper.COL_CANDIDATE_NAME,candidateApplyInfo.getCandidateName());
        contentValues.put(CandidateDatabaseHelper.COL_COMPANY_NAME,candidateApplyInfo.getCompanyName());
        contentValues.put(CandidateDatabaseHelper.COL_JOB_POST,candidateApplyInfo.getJobPost());
        contentValues.put(CandidateDatabaseHelper.COL_JOB_NATURE,candidateApplyInfo.getJobNature());
        contentValues.put(CandidateDatabaseHelper.COL_JOB_LOCATION,candidateApplyInfo.getJobLocation());
        contentValues.put(CandidateDatabaseHelper.COL_JOB_SALARY,candidateApplyInfo.getJobSalary());
        contentValues.put(CandidateDatabaseHelper.COL_JOB_DEADLINE_DATE,candidateApplyInfo.getJobDeadlineDate());

        int updated = database.update
                (
                        CandidateDatabaseHelper.TABLE_CANDIDATE_INFO,
                        contentValues,
                        CandidateDatabaseHelper.COL_ID + " = " + candidateId,
                        null
                );

        this.close();

        if (updated>0){
            return true;
        }
        else {
            return false;
        }

    }

}
