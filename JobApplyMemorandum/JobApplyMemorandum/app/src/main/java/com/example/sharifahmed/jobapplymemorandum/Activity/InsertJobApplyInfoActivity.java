package com.example.sharifahmed.jobapplymemorandum.Activity;

import android.content.Intent;
import android.database.CursorIndexOutOfBoundsException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sharifahmed.jobapplymemorandum.DatabaseOperation.CandidateDatabaseOperation;
import com.example.sharifahmed.jobapplymemorandum.Model.CandidateApplyInfo;
import com.example.sharifahmed.jobapplymemorandum.R;
import com.example.sharifahmed.jobapplymemorandum.ShPreferrence.SHaredPreferrence;

import java.util.ArrayList;

public class InsertJobApplyInfoActivity extends AppCompatActivity {


    private EditText candidateNameET;
    private EditText companyNameET;
    private EditText jobPostET;
    private EditText jobNatureET;
    private EditText jobLocationET;
    private EditText jobSalaryET;
    private EditText jobDeadLineDateET;
    Button saveButton;

    private CandidateApplyInfo candidateApplyInfo;
    private ArrayList<CandidateApplyInfo> candidateApplyInfos;
    private CandidateDatabaseOperation candidateDatabaseOperation;

    SHaredPreferrence sHaredPreferrence;

    int candidateId;

    boolean statusUpdate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        //**************************** For Back Button ***************************************************

        if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        //**************************** End Back Button Work **********************************************

        sHaredPreferrence = new SHaredPreferrence(this);

        candidateNameET = (EditText) findViewById(R.id.candidateNameEditTextId);
        companyNameET = (EditText) findViewById(R.id.companyNameEditTextId);
        jobPostET = (EditText) findViewById(R.id.jobPostEditTextId);
        jobNatureET = (EditText) findViewById(R.id.jobNatureEditTextId);
        jobLocationET = (EditText) findViewById(R.id.jobLocationEditTextId);
        jobSalaryET = (EditText) findViewById(R.id.jobSalaryEditTextId);
        jobDeadLineDateET = (EditText) findViewById(R.id.jobDeadlineDateEditTextId);
        saveButton = (Button)findViewById(R.id.saveButton);

        candidateDatabaseOperation = new CandidateDatabaseOperation(this);//Here Is The DataBase Initialize

        //candidateId = sHaredPreferrence.retriveMethod();
        candidateId = getIntent().getIntExtra("id",0);



//Here The Data Is From Database By Single Id Of JobApplyListDetails



        if(candidateId>0){
            try{
                candidateApplyInfo=candidateDatabaseOperation.getCandidateInfoById(candidateId);

                candidateNameET.setText(candidateApplyInfo.getCandidateName());
                companyNameET.setText(candidateApplyInfo.getCompanyName());
                jobPostET.setText(candidateApplyInfo.getJobPost());
                jobNatureET.setText(candidateApplyInfo.getJobNature());
                jobLocationET.setText(candidateApplyInfo.getJobLocation());
                jobSalaryET.setText(candidateApplyInfo.getJobSalary());
                jobDeadLineDateET.setText(candidateApplyInfo.getJobDeadlineDate());

                saveButton.setText("update");
            }
            catch (CursorIndexOutOfBoundsException ex){
                Toast.makeText(getApplicationContext(),"No Data You Want To Update",Toast.LENGTH_SHORT).show();
                Intent goInsertIntent = new Intent(getApplicationContext(),InsertJobApplyInfoActivity.class);
                startActivity(goInsertIntent);
            }

        }


    }

    public void saveCandidateInfo(View view) {

        String candidateName = candidateNameET.getText().toString();
        String companyName = companyNameET.getText().toString();
        String jobPost = jobPostET.getText().toString();
        String jobNature = jobNatureET.getText().toString();
        String jobLocation = jobLocationET.getText().toString();
        String jobSalary = jobSalaryET.getText().toString();
        String jobDeadLineDate = jobDeadLineDateET.getText().toString();

        boolean isValid = isValidData(candidateName,companyName,jobPost,jobNature,jobLocation,jobSalary,jobDeadLineDate);

        if(isValid){

            candidateApplyInfo = new CandidateApplyInfo(candidateName,companyName,jobPost,jobNature,jobLocation,jobSalary,jobDeadLineDate);

            if (candidateId>0){

                statusUpdate=candidateDatabaseOperation.updateCandidateInfoById(candidateId,candidateApplyInfo);
                if(statusUpdate) {
                    Toast.makeText(InsertJobApplyInfoActivity.this,"Candidate Info Update Successful", Toast.LENGTH_LONG).show();
                    Intent goJobApplyListIntent = new Intent(getApplicationContext(),JobApplyListActivity.class);
                    startActivity(goJobApplyListIntent);
                }else {
                    Toast.makeText(InsertJobApplyInfoActivity.this,"Candidate Info Update Failed", Toast.LENGTH_LONG).show();
                }

            }
            else
            {
                boolean insertCandidate = candidateDatabaseOperation.addCandidate(candidateApplyInfo);
                if(insertCandidate){
                    Toast.makeText(InsertJobApplyInfoActivity.this, "Candidate Info Inserted Successfully", Toast.LENGTH_LONG).show();
                    Intent goJobApplyListIntent = new Intent(getApplicationContext(),JobApplyListActivity.class);
                    startActivity(goJobApplyListIntent);
                }
                else {
                    Toast.makeText(InsertJobApplyInfoActivity.this, "Candidate Info Inserted Failed", Toast.LENGTH_LONG).show();
                }

            }
        }
        else {
            Toast.makeText(InsertJobApplyInfoActivity.this, "Please Fill Required The Input Field", Toast.LENGTH_LONG).show();
        }
    }

    public boolean isValidData(String candidateName, String companyName, String jobPost,String jobNature,String jobLocation,String jobSalary,String jobDeadLineDate){

        //String regExprtn = "^[a-zA-Z]+$";

        if(candidateName.length()<=0 ){
            candidateNameET.setError("Enter Candidate Name");
            return false;
        }
        else if(companyName.length()<=0 ){
            companyNameET.setError("Enter Company Name");
            return false;
        }
        else if(jobPost.length()<=0 ){
            jobPostET.setError("Enter Job Post");
            return false;
        }
        else if(jobNature.length()<=0){
            jobNatureET.setError("Enter Job Nature");
            return false;
        }
        else if(jobLocation.length()<=0  ){
            jobLocationET.setError("Enter Job Location");
            return false;
        }
        else if(jobSalary.length()<=0){
            jobSalaryET.setError("Enter Job Salary");
            return false;
        }
        else if(jobDeadLineDate.length()<=0 ){
            jobDeadLineDateET.setError("Enter Job App Deadline Date");
            return false;
        }
        else{
            return true;
        }
    }




    //**************************** For Menu Item Work ***************************************************8
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = new MenuInflater(InsertJobApplyInfoActivity.this);
        menuInflater.inflate(R.menu.job_apply_list_insert_menu,menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){

            case R.id.showJobApplyIstId :
                Intent goJobApplyListIntent = new Intent(getApplicationContext(),JobApplyListActivity.class);
                startActivity(goJobApplyListIntent);
                break;
            case android.R.id.home : //For Back Button
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }
//**********************************End Menu Item Work**********************************************


}
