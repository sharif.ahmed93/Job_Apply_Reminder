package com.example.sharifahmed.jobapplymemorandum.DatabaseHelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CandidateDatabaseHelper extends SQLiteOpenHelper {

    static final String DATABASE_NAME = "candidate_info_db";//HERE IS THE DATABASE

    public static final String TABLE_CANDIDATE_INFO       = "candidate_info_table";//HERE IS THE 'candidate_info_db' DATABASE TABLE NAME


    public static final String COL_ID                     = "candidate_id";

    public static final String COL_CANDIDATE_NAME         = "candidate_name";

    public static final String COL_COMPANY_NAME           = "candidate_company_name";

    public static final String COL_JOB_POST               = "candidate_job_post";

    public static final String COL_JOB_NATURE             = "candidate_job_nature";

    public static final String COL_JOB_LOCATION           = "candidate_job_location";

    public static final String COL_JOB_SALARY             = "candidate_job_salary";

    public static final String COL_JOB_DEADLINE_DATE      = "candidate_job_deadline_date";



    static final int DATABASE_VERSION = 1;


    String CREATE_TABLE_CANDIDATE_INFO = " CREATE TABLE " + TABLE_CANDIDATE_INFO +
                                         " ( " + COL_ID + " INTEGER PRIMARY KEY," +
                                                COL_CANDIDATE_NAME + " TEXT," +
                                                COL_COMPANY_NAME + " TEXT," +
                                                COL_JOB_POST + " TEXT," +
                                                COL_JOB_NATURE + " TEXT," +
                                                COL_JOB_LOCATION + " TEXT," +
                                                COL_JOB_SALARY + " TEXT," +
                                                COL_JOB_DEADLINE_DATE + " TEXT )";

    public CandidateDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_CANDIDATE_INFO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

































/*
    static final String DATABASE_NAME="contact_manager";
    static final int DATABASE_VERSION=1;

    static final String TABLE_CONTACT="contact_info";
    static final String COL_ID="id";
    static final String COL_NAME="name";
    static final String COL_PHONE="phone";

    static final String CREATE_TABLE_CONTACT="create table "+TABLE_CONTACT+"( "+COL_ID+" integer primary key, "+COL_NAME+" text, "+COL_PHONE+" text);";

    public CandidateDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_CONTACT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("drop table if exist"+TABLE_CONTACT);
        onCreate(sqLiteDatabase);
    }
    */
}
