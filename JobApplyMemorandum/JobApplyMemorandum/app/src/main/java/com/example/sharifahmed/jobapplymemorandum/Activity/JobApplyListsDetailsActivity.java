package com.example.sharifahmed.jobapplymemorandum.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.sharifahmed.jobapplymemorandum.DatabaseOperation.CandidateDatabaseOperation;
import com.example.sharifahmed.jobapplymemorandum.R;
import com.example.sharifahmed.jobapplymemorandum.ShPreferrence.SHaredPreferrence;

public class JobApplyListsDetailsActivity extends AppCompatActivity {


    private TextView candidateName;
    private TextView companyName;
    private TextView jobPost;
    private TextView jobNature;
    private TextView jobLocation;
    private TextView jobSalary;
    private TextView jobDeadLineDate;
    int id;

    CandidateDatabaseOperation candidateDatabaseOperation;
    SHaredPreferrence sHaredPreferrence;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_apply_lists_details);



        //**************************** For Back Button ***************************************************

        if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        //**************************** End Back Button Work **********************************************




        candidateDatabaseOperation = new CandidateDatabaseOperation(this);


        candidateName = (TextView) findViewById(R.id.candidateNameId);
        companyName = (TextView) findViewById(R.id.companyNameId);
        jobPost = (TextView) findViewById(R.id.jobPostId);
        jobNature = (TextView) findViewById(R.id.jobNatureId);
        jobLocation = (TextView) findViewById(R.id.jobLocationId);
        jobSalary = (TextView) findViewById(R.id.jobSalaryId);
        jobDeadLineDate = (TextView) findViewById(R.id.jobDeadlineDateId);


        candidateName.setText(getIntent().getStringExtra("CandidateName"));
        companyName.setText(getIntent().getStringExtra("CompanyName"));
        jobPost.setText(getIntent().getStringExtra("JobPost"));
        jobNature.setText(getIntent().getStringExtra("JobNature"));
        jobLocation.setText(getIntent().getStringExtra("JobLocation"));
        jobSalary.setText(getIntent().getStringExtra("JobSalary"));
        jobDeadLineDate.setText(getIntent().getStringExtra("JobDeadlineDate"));

        //sHaredPreferrence = new SHaredPreferrence(this);
        //sHaredPreferrence.saveUserData(getIntent().getIntExtra("Id",0));

        //id = sHaredPreferrence.retriveMethod();

        id = getIntent().getIntExtra("Id",0);

    }

    public void editCandidateInfo(View view) {

        if(id!=0){
            Intent updateIntent=new Intent(JobApplyListsDetailsActivity.this,InsertJobApplyInfoActivity.class);
            updateIntent.putExtra("id",id);
            startActivity(updateIntent);
        }

    }

    public void deleteCandidateInfo(View view) {

        candidateDatabaseOperation.deleteCandidate(id);
        Intent goShowApplyListIntent=new Intent(getApplicationContext(),JobApplyListActivity.class);
        startActivity(goShowApplyListIntent);

    }



    //**************************** For Menu Item Work ***************************************************8
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = new MenuInflater(JobApplyListsDetailsActivity.this);
        menuInflater.inflate(R.menu.job_apply_list_menu,menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){

            //For Back Button
            case android.R.id.home :
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }
    //**********************************End Menu Item Work**********************************************
}
