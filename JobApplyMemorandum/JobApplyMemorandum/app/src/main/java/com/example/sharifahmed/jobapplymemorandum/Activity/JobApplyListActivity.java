package com.example.sharifahmed.jobapplymemorandum.Activity;

import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sharifahmed.jobapplymemorandum.Adapter.RecyclerAdapter;
import com.example.sharifahmed.jobapplymemorandum.DatabaseOperation.CandidateDatabaseOperation;
import com.example.sharifahmed.jobapplymemorandum.Model.CandidateApplyInfo;
import com.example.sharifahmed.jobapplymemorandum.R;

import java.util.ArrayList;

public class JobApplyListActivity extends AppCompatActivity {

    CandidateDatabaseOperation candidateDatabaseOperation;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter recyclerAdapter;
    private RecyclerView.LayoutManager layoutManager;

    ImageView welComeImageView;
    TextView insertTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_apply_list);


       welComeImageView = (ImageView) findViewById(R.id.welComeImageViewId);
       // insertTextView = (TextView) findViewById(R.id.insertTextViewId);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewId);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);



        candidateDatabaseOperation = new CandidateDatabaseOperation(this);

        int totalContact = candidateDatabaseOperation.getAllCandidateInfo().size();
        if(totalContact != 0){
            recyclerAdapter = new RecyclerAdapter(candidateDatabaseOperation.getAllCandidateInfo(),this);
            recyclerView.setAdapter(recyclerAdapter);
        }
        else {
            Toast.makeText(JobApplyListActivity.this, "There Is No Data To Be Display", Toast.LENGTH_LONG).show();
//            insertTextView.setText("Click Here For Insert");
            welComeImageView.setImageResource(R.drawable.wel);



            welComeImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent goJobApplyListIntent = new Intent(getApplicationContext(), InsertJobApplyInfoActivity.class);
                    startActivity(goJobApplyListIntent);
                }
            });
        }

    }


    //**************************** For Menu Item Work ***************************************************8
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = new MenuInflater(JobApplyListActivity.this);
        menuInflater.inflate(R.menu.job_apply_list_menu,menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){


            case R.id.jobApplyInfoId :
                Intent goJobApplyListIntent = new Intent(getApplicationContext(),InsertJobApplyInfoActivity.class);
                startActivity(goJobApplyListIntent);
                break;

        }
        return super.onOptionsItemSelected(item);
    }
//**********************************End Menu Item Work**********************************************
}
